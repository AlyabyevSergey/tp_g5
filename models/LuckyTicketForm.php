<?php


namespace app\models;


use yii\base\Model;

class LuckyTicketForm extends Model
{

    public $min_number;
    public $max_number;

    public function attributeLabels()
    {
        return [
            'min_number' => 'N - from',
            'max_number' => 'N - to'
        ];
    }

    public function rules()
    {
        return [
            [ ['min_number', 'max_number'],  'required'],
              ['min_number', 'integer', 'min' => 1],
              ['max_number', 'integer', 'max' => 999999],
        ];
    }



    /*
     * Возвращает количество счастливых билетов в диапазоне
     *
     * */

    public function getCountLuckyTicket()
     {
         $lucky_ticket = 0;
         for ($i = $this->min_number; $i <= $this->max_number; $i++) {
            if ($this->isLuckyTicket($i)){
               $lucky_ticket++;}
         }

         return $lucky_ticket;
     }

     /*
      * Проверяет счастливый ли билет (1/null)
      * Счастливым билетом считать комбинацию при которой сумма первых трех цифр равна сумме последних трех
      *
      *  000001 - первые три 000, последние три 001
      *  123456 - первые три 123, последние три 456
      * */

     private function isLuckyTicket($number)
      {

         $first_3_dig  = floor($number/1000);
         $second_3_dig = round(($number/1000 - floor($number/1000))*1000);

         $first_sum_dig  = $this->getSumDigital($first_3_dig);
         $second_sum_dig = $this->getSumDigital($second_3_dig);

         return ($first_sum_dig == $second_sum_dig);
      }



      /*
       * Возращает сумму цифр числа
       * Сумма представлена лишь 1 цифрой
       *   4 + 5 + 6 = 15 => 1 + 5 = 6
       */

     private function getSumDigital($value){
        $value = strval($value);
        $sum = 0;
        for ($i = 0; $i < strlen($value); $i++) {
             $sum += $value[$i];
         }

        //Если сумма цифр является не 1-разрядной, еще раз прогоняем процедуру
        if ($sum > 9){
            $sum = $this->getSumDigital($sum);
        }

        return $sum;
     }

}