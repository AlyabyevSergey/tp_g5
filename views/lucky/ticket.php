<h1>Счастливый билет</h1>

<?php
use yii\widgets\ActiveForm;
use yii\helpers\Html;
use Yii;

$this->registerCssFile('@web/css/lucky.css');

$form = ActiveForm::begin(['id' => 'NumbersTicket']);
echo $form->field($model, 'min_number');
echo $form->field($model, 'max_number');
echo Html::submitButton('Run', ['class' => 'btn btn-success']);
ActiveForm::end();
