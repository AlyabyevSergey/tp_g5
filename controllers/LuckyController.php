<?php


namespace app\controllers;

use app\models\LuckyTicketForm;
use yii\web\Controller;
use Yii;

class LuckyController extends Controller
{

    public function actionTicket()
      {
          $model = new LuckyTicketForm();
          if ($model->load(Yii::$app->request->post())) {
             Yii::$app->session->setFlash('success', 'Number of tickets: '.$model->getCountLuckyTicket());
          }
          return $this->render('ticket', compact('model'));
      }

}